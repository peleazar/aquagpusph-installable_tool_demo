ifeq ($(AQUAGPUSPH_INCLUDE),"")
        AQUAGPUSPH_INCLUDE=/usr/include/AQUAgpusph
endif

tool.so: tool.cpp
	gcc -fPIC -shared -rdynamic -o tool.so tool.cpp -I./ -I$(AQUAGPUSPH_INCLUDE) -I$(AQUAGPUSPH_INCLUDE)/include

all: tool.so

clean:
	rm -f tool.so
